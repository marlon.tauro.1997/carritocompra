package carrito.compras.com.carritoCompras.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import carrito.compras.com.carritoCompras.domain.DetalleVenta;


public interface DetalleVentaRepository extends JpaRepository<DetalleVenta, Long> {
    // Puedes agregar consultas personalizadas si es necesario
	@Query("from DetalleVenta dv where dv.venta.id = :codigoVenta")
	List<DetalleVenta> findByVenta(@Param("codigoVenta") Long codigoVenta);
}
