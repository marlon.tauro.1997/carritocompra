package carrito.compras.com.carritoCompras.dto;

import java.time.LocalDate;
import java.util.List;

public class VentaDTO {

	    private Long id;
	    private Object cliente;
	    private LocalDate fecha;
	    private List<DetalleVentaDTO> lineaVenta;
	    
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Object getCliente() {
			return cliente;
		}
		public void setCliente(Object cliente) {
			this.cliente = cliente;
		}
		public LocalDate getFecha() {
			return fecha;
		}
		public void setFecha(LocalDate fecha) {
			this.fecha = fecha;
		}
		public List<DetalleVentaDTO> getLineaVenta() {
			return lineaVenta;
		}
		public void setLineaVenta(List<DetalleVentaDTO> lineaVenta) {
			this.lineaVenta = lineaVenta;
		}
		
}
