package carrito.compras.com.carritoCompras.dto;


public class DetalleVentaDTO {

    private Long id;
    private Object venta;
    private Object producto;
    private int cantidad;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Object getVenta() {
		return venta;
	}
	public void setVenta(Object venta) {
		this.venta = venta;
	}
	public Object getProducto() {
		return producto;
	}
	public void setProducto(Object producto) {
		this.producto = producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
    
}
