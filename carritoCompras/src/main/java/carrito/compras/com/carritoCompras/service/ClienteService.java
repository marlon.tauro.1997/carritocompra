package carrito.compras.com.carritoCompras.service;

import carrito.compras.com.carritoCompras.dto.ClienteDTO;
public interface ClienteService {
    // Métodos para operaciones CRUD
	public ClienteDTO guardarCliente(ClienteDTO clienteDTO);
}
