package carrito.compras.com.carritoCompras.Controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import carrito.compras.com.carritoCompras.dto.VentaDTO;
import carrito.compras.com.carritoCompras.service.VentaService;


@RestController
@RequestMapping("/ventas")
public class VentaController {

    @Autowired
    private VentaService ventaService;

    @GetMapping("/fecha/{fecha}")
    public ResponseEntity<List<VentaDTO>> obtenerVentasEnFecha(@PathVariable LocalDate fecha) {
        List<VentaDTO> ventas = ventaService.obtenerVentasEnFecha(fecha);
        return new ResponseEntity<>(ventas, HttpStatus.OK);
    }
    
    @PostMapping("/venta")
    public ResponseEntity<VentaDTO> guardarVenta(@RequestBody VentaDTO ventaDto){
    	VentaDTO ventaDTO = ventaService.guardarVenta(ventaDto);
    	return new ResponseEntity<>(ventaDTO, HttpStatus.OK);
    }

}
