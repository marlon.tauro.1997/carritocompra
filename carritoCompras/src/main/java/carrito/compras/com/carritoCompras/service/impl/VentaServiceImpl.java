package carrito.compras.com.carritoCompras.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import carrito.compras.com.carritoCompras.domain.Cliente;
import carrito.compras.com.carritoCompras.domain.DetalleVenta;
import carrito.compras.com.carritoCompras.dto.DetalleVentaDTO;
import carrito.compras.com.carritoCompras.domain.Venta;
import carrito.compras.com.carritoCompras.dto.VentaDTO;
import carrito.compras.com.carritoCompras.repository.ClienteRepository;
import carrito.compras.com.carritoCompras.repository.DetalleVentaRepository;
import carrito.compras.com.carritoCompras.repository.ProductoRepository;
import carrito.compras.com.carritoCompras.repository.VentaRepository;
import carrito.compras.com.carritoCompras.service.VentaService;
import jakarta.persistence.EntityNotFoundException;

@Service
public class VentaServiceImpl implements VentaService {
	
	@Autowired
	private ClienteRepository clienteRepository;

    @Autowired
    private VentaRepository ventaRepository;
    
    @Autowired
	private ProductoRepository productoRepository;
    
    @Autowired
    private DetalleVentaRepository detalleVentaRepository;

    @Override
    public List<VentaDTO> obtenerVentasEnFecha(LocalDate fecha) {
        List<Venta> listaVentas = ventaRepository.findByFecha(fecha);
        List<VentaDTO> listaVentasDTO = new ArrayList<VentaDTO>();
        for(Venta venta : listaVentas) {
        	VentaDTO ventaDto = new VentaDTO();
        	ventaDto.setId(venta.getId());
        	ventaDto.setCliente(venta.getCliente().getId());
        	ventaDto.setFecha(venta.getFecha());
        	List<DetalleVenta> lineaVenta = detalleVentaRepository.findByVenta(venta.getId());
        	List<DetalleVentaDTO> lineaVentaDto = new ArrayList<DetalleVentaDTO>();
        	for(DetalleVenta detalleVenta : lineaVenta) {
        		DetalleVentaDTO detalleVentaDTO = new DetalleVentaDTO();
        		detalleVentaDTO.setId(detalleVenta.getId());
        		detalleVentaDTO.setVenta(detalleVenta.getVenta().getId());
        		detalleVentaDTO.setProducto(detalleVenta.getProducto().getId());
        		detalleVentaDTO.setCantidad(detalleVenta.getCantidad());
        		lineaVentaDto.add(detalleVentaDTO);
        	}
        	ventaDto.setLineaVenta(lineaVentaDto);
        	listaVentasDTO.add(ventaDto);
        }
        
        return listaVentasDTO;
    }

	@Override
	public VentaDTO obtenerVentaPorId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VentaDTO guardarVenta(VentaDTO ventaDTO) {
		Venta venta = new Venta();
		Cliente cliente = clienteRepository.getById(ventaDTO.getId());
		venta.setId(ventaDTO.getId());
		venta.setCliente(cliente);
		venta.setFecha(ventaDTO.getFecha());
		for(DetalleVentaDTO detalleVentaDTO : ventaDTO.getLineaVenta()) {
			DetalleVenta detalleVenta = new DetalleVenta();
			detalleVenta.setId(detalleVentaDTO.getId());
			detalleVenta.setVenta(ventaRepository.findById(Long.valueOf(detalleVentaDTO.getVenta().toString())).orElse(null));
			detalleVenta.setProducto(productoRepository.findById(Long.valueOf(detalleVentaDTO.getProducto().toString())).orElse(null));
			detalleVenta.setCantidad(detalleVentaDTO.getCantidad());
			detalleVentaRepository.save(detalleVenta);
		}
		
		ventaRepository.save(venta);
		return ventaDTO;
	}
}
