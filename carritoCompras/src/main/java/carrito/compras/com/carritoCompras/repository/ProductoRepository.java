package carrito.compras.com.carritoCompras.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import carrito.compras.com.carritoCompras.domain.Producto;


public interface ProductoRepository extends JpaRepository<Producto, Long> {
    // Puedes agregar consultas personalizadas si es necesario
}
