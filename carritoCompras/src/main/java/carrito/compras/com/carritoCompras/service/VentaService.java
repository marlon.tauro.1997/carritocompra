package carrito.compras.com.carritoCompras.service;

import java.time.LocalDate;
import java.util.List;

import carrito.compras.com.carritoCompras.dto.VentaDTO;

public interface VentaService {
	
    // Otros servicios e inyecciones necesarias

    public List<VentaDTO> obtenerVentasEnFecha(LocalDate fecha);

    public VentaDTO obtenerVentaPorId(Long id);
    
    public VentaDTO guardarVenta(VentaDTO ventaDTO);

}
