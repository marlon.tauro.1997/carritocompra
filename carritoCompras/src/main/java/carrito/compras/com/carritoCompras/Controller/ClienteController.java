package carrito.compras.com.carritoCompras.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import carrito.compras.com.carritoCompras.dto.ClienteDTO;
import carrito.compras.com.carritoCompras.service.ClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    // Endpoints para operaciones CRUD
    
    @PostMapping("/producto")
    public ResponseEntity<ClienteDTO> guardarProdducto(@RequestBody ClienteDTO clienteDto){
    	ClienteDTO clienteDTO = clienteService.guardarCliente(clienteDto);
    	return new ResponseEntity<>(clienteDTO, HttpStatus.OK);
    }
}

