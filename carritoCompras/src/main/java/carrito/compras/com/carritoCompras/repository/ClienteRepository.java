package carrito.compras.com.carritoCompras.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import carrito.compras.com.carritoCompras.domain.Cliente;



public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    // Puedes agregar consultas personalizadas si es necesario
}
