package carrito.compras.com.carritoCompras.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import carrito.compras.com.carritoCompras.domain.Cliente;
import carrito.compras.com.carritoCompras.domain.DetalleVenta;
import carrito.compras.com.carritoCompras.domain.Producto;
import carrito.compras.com.carritoCompras.dto.DetalleVentaDTO;
import carrito.compras.com.carritoCompras.dto.ProductoDTO;
import carrito.compras.com.carritoCompras.dto.VentaDTO;
import carrito.compras.com.carritoCompras.repository.ProductoRepository;

public class ProductoServiceImpl {

	@Autowired
	private ProductoRepository productoRepository;
    
	public ProductoDTO guardarProducto(ProductoDTO productoDTO) {
		Producto producto= new Producto();
		producto.setId(productoDTO.getId());
		producto.setNombre(productoDTO.getNombre());
		producto.setPrecio(productoDTO.getPrecio());
		
		
		productoRepository.save(producto);
		return productoDTO;
	}
}
