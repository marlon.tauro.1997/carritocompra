package carrito.compras.com.carritoCompras.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import carrito.compras.com.carritoCompras.dto.ProductoDTO;
import carrito.compras.com.carritoCompras.dto.VentaDTO;
import carrito.compras.com.carritoCompras.service.ProductoService;

@RestController
@RequestMapping("/productos")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    // Endpoints para operaciones CRUD
    
    @PostMapping("/producto")
    public ResponseEntity<ProductoDTO> guardarProdducto(@RequestBody ProductoDTO productoDto){
    	ProductoDTO productoDTO = productoService.guardarProducto(productoDto);
    	return new ResponseEntity<>(productoDTO, HttpStatus.OK);
    }
}
