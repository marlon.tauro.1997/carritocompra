package carrito.compras.com.carritoCompras.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import carrito.compras.com.carritoCompras.domain.Cliente;
import carrito.compras.com.carritoCompras.dto.ClienteDTO;
import carrito.compras.com.carritoCompras.repository.ClienteRepository;

public class ClienteServiceImpl {

	@Autowired
	private ClienteRepository clienteRepository;
    
	public ClienteDTO guardarCliente(ClienteDTO clienteDTO) {
		Cliente cliente= new Cliente();
		cliente.setId(clienteDTO.getId());
		cliente.setNombres(clienteDTO.getNombres());
		cliente.setApellidos(clienteDTO.getApellidos());
		cliente.setDni(clienteDTO.getDni());
		cliente.setTelefono(clienteDTO.getTelefono());
		cliente.setEmail(clienteDTO.getEmail());
		
		
		clienteRepository.save(cliente);
		return clienteDTO;
	}
}
