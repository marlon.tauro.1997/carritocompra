package carrito.compras.com.carritoCompras.service;

import carrito.compras.com.carritoCompras.dto.ProductoDTO;

public interface ProductoService {
    // Métodos para operaciones CRUD
	
	public ProductoDTO guardarProducto(ProductoDTO productoDTO);
}
