package carrito.compras.com.carritoCompras.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import carrito.compras.com.carritoCompras.domain.Venta;

public interface VentaRepository extends JpaRepository<Venta, Long> {
    // Puedes agregar consultas personalizadas si es necesario
	List<Venta> findByFecha(LocalDate fecha);
}

